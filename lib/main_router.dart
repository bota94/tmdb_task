import 'package:auto_route/annotations.dart';
import 'package:tmdb_task/UI/home_screen.dart';
import 'package:tmdb_task/UI/login_screen.dart';
import 'package:tmdb_task/UI/movie_details_screen.dart';
import 'package:tmdb_task/UI/now_playing_movies_screen.dart';
import 'package:tmdb_task/UI/splash_screen.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Screen,Route',
  routes: <AutoRoute>[
    AutoRoute(page: SplashScreen, initial: true),
    AutoRoute(page: LoginScreen),
    AutoRoute(page: NowPlayingMoviesScreen),
    AutoRoute(page: MovieDetailsScreen),
    AutoRoute(page: HomeScreen),
  ],
)
class $AppRouter {}
