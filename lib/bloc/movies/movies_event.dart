abstract class MoviesEvent{}

class MovieListRequested extends MoviesEvent{
  int page;
  MovieListRequested(this.page);
}

class AddMovieToWatchListTapped extends MoviesEvent{
  int movieID;
  AddMovieToWatchListTapped(this.movieID);
}

class RemoveMovieFromWatchListTapped extends MoviesEvent{
  int movieID;
  RemoveMovieFromWatchListTapped(this.movieID);
}

class WatchListRequested extends MoviesEvent{
  int page;
  WatchListRequested(this.page);
}

class LogStatusRequested extends MoviesEvent{}