import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/bloc/movies/bloc.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/resources/strings.dart';

class MovieDetailsScreen extends StatefulWidget {
  final Movie movie;
  final bool inWatchList;
  const MovieDetailsScreen(
      {Key? key, required this.movie, required this.inWatchList})
      : super(key: key);

  @override
  State<MovieDetailsScreen> createState() => _MovieDetailsScreenState();
}

class _MovieDetailsScreenState extends State<MovieDetailsScreen> {
  final MoviesBloc _moviesBloc = GetIt.instance<MoviesBloc>();
  late StreamSubscription _moviesSub;
  bool inWatchList = false;
  bool isLogged = false;

  @override
  void initState() {
    setState(() {
      inWatchList = widget.inWatchList;
    });
    _moviesSub = _moviesBloc.moviesStateSubject.listen((state) {
      if (state is MovieAddedToWatchList) {
        setState(() {
          inWatchList = true;
        });
      }
      if (state is MovieRemovedFromWatchList) {
        setState(() {
          inWatchList = false;
        });
      }
      if (state is LoggedStatusIs) {
        setState(() {
          isLogged = state.isUserLogged;
        });
      }
    });
    _moviesBloc.dispatch(LogStatusRequested());
    super.initState();
  }

  @override
  void dispose() {
    _moviesSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.movie.title),
        actions: [
          Padding(
            padding: const EdgeInsetsDirectional.only(end: 10.0),
            child: Visibility(
              visible: isLogged,
              child: InkWell(
                onTap: () {
                  if (inWatchList) {
                    _moviesBloc.dispatch(
                        RemoveMovieFromWatchListTapped(widget.movie.id));
                  } else {
                    _moviesBloc
                        .dispatch(AddMovieToWatchListTapped(widget.movie.id));
                  }
                },
                child: Icon(
                  inWatchList ? Icons.remove : Icons.add,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                  margin: const EdgeInsetsDirectional.only(end: 8),
                  width: 160,
                  height: 190,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(
                          Links.imageBaseLink + widget.movie.poster_path,
                        ),
                        fit: BoxFit.cover),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  widget.movie.overview,
                  style: const TextStyle(fontSize: 18),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(AppStrings.releaseDate,
                      style: TextStyle(fontSize: 16)),
                  Text(widget.movie.release_date,
                      style: const TextStyle(fontSize: 16))
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(AppStrings.originalLang,
                        style: TextStyle(fontSize: 16)),
                    Text(widget.movie.original_language.toUpperCase(),
                        style: const TextStyle(fontSize: 16))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
