import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesProvider {
  late SharedPreferences prefs;
  static const String _sessionID = "SESSIONID";
  static const String _accountID = "ACCOUNTID";

  void setSessionID(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(_sessionID, id);
  }

  Future<String?> getSessionID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? id = prefs.getString(_sessionID);
    return id;
  }

  void setAccountID(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(_accountID, id);
  }

  Future<int?> getAccountID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int? accountID = prefs.getInt(_accountID);
    return accountID;
  }
}
