import 'dart:async';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/bloc/auth/bloc.dart';
import 'package:tmdb_task/main_router.gr.dart';
import 'package:tmdb_task/resources/strings.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  InputBorder border =
      const OutlineInputBorder(borderSide: BorderSide(color: Colors.grey));
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();
  bool buttonLoading = false;
  bool showLoginError = false;
  late StreamSubscription _authSub;

  @override
  void initState() {
    _authSub = _authBloc.authStateSubject.listen((state) {
      if (state is LoginSuccess) {
        AutoRouter.of(context).pushAndPopUntil(const HomeRoute(),
            predicate: (r) => false);
      }
      if (state is LoginErrorIs) {
        setState(() {
          buttonLoading = false;
          showLoginError = true;
        });
        showDialog(
            context: context,
            builder: (b) {
              return _buildCantLoginDialog();
            });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _authSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 100),
              child: Center(
                  child: Text(AppStrings.appTitle,
                      style: TextStyle(
                          fontSize: 28, fontWeight: FontWeight.bold))),
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  _buildTextField(
                      AppStrings.username, _usernameController, false),
                  _buildTextField(
                      AppStrings.password, _passwordController, true),
                ],
              ),
            ),
            _buildButton(),
            _buildAccessAnonymouslyWidget()
          ],
        ),
      ),
    );
  }

  Widget _buildTextField(
      String hint, TextEditingController controller, bool obscure) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 16),
      child: TextFormField(
        obscureText: obscure,
        controller: controller,
        validator: (value) {
          if (value?.isEmpty ?? false) {
            return "$hint ${AppStrings.isRequired}";
          }
        },
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 10.0,
            vertical: 0,
          ),
          hintText: hint,
          hintStyle: const TextStyle(color: Colors.grey),
          enabledBorder: border,
          border: border,
          focusedBorder: border,
          errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red)),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 18, horizontal: 16),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: TextButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.blue,
          padding: const EdgeInsets.symmetric(vertical: 14),
        ),
        onPressed: () {
          if (_formKey.currentState?.validate() ?? false) {
            setState(() {
              buttonLoading = true;
              showLoginError = false;
            });
            _authBloc.dispatch(LoginTapped(
                _usernameController.text, _passwordController.text));
          }
        },
        child: Center(
          child: buttonLoading
              ? _loader()
              : const Text(
                  AppStrings.login,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
        ),
      ),
    );
  }

  Widget _loader() {
    return const SpinKitThreeBounce(
      color: Colors.white,
      size: 20,
    );
  }

  Widget _buildCantLoginDialog() {
    return Dialog(
      insetPadding: const EdgeInsets.all(30),
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Text(
                AppStrings.loginError,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      AutoRouter.of(context).pop();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadiusDirectional.only(
                              bottomEnd: Radius.circular(10),
                              bottomStart: Radius.circular(10)),
                          color: Colors.blue),
                      child: const Center(
                          child: Text(
                        AppStrings.ok,
                        style: TextStyle(color: Colors.white),
                      )),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildAccessAnonymouslyWidget() {
    return InkWell(
      child: const Text(
        AppStrings.useAnonymously,
        style: TextStyle(fontSize: 18, color: Colors.blue),
      ),
      onTap: () {
        AutoRouter.of(context).push(const NowPlayingMoviesRoute());
      },
    );
  }
}
