import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/UI/favorite_movie_widget.dart';
import 'package:tmdb_task/bloc/movies/bloc.dart';
import 'package:tmdb_task/models/movie.dart';
import 'package:tmdb_task/my_app.dart';
import 'package:tmdb_task/resources/strings.dart';

class WatchListMoviesScreen extends StatefulWidget {
  const WatchListMoviesScreen({Key? key}) : super(key: key);

  @override
  State<WatchListMoviesScreen> createState() => _WatchListMoviesScreenState();
}

class _WatchListMoviesScreenState extends State<WatchListMoviesScreen>
    with RouteAware {
  final MoviesBloc _moviesBloc = GetIt.instance<MoviesBloc>();
  late StreamSubscription _moviesSub;
  List<Movie> watchListMovies = [];
  bool loading = true;
  int cursor = 1;
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    scrollController = ScrollController()..addListener(_scrollListener);

    _moviesSub = _moviesBloc.moviesStateSubject.listen((state) {
      if (state is WatchListMoviesAre) {
        setState(() {
          watchListMovies = state.watchListMovies;
          loading = false;
        });
      }
      if (state is MovieRemovedFromWatchList) {
        setState(() {
          watchListMovies = state.watchListMovies;
        });
      }
    });
    _moviesBloc.dispatch(MovieListRequested(cursor));
    _moviesBloc.dispatch(WatchListRequested(cursor));
    super.initState();
  }

  @override
  void dispose() {
    _moviesSub.cancel();
    MyApp.routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    MyApp.routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void didPopNext() {
    _moviesBloc.dispatch(MovieListRequested(cursor));
    _moviesBloc.dispatch(WatchListRequested(cursor));
  }

  void _scrollListener() {
    if (scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) {
      setState(() {
        cursor = cursor + 1;
      });
      _moviesBloc.dispatch(MovieListRequested(cursor));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: InkWell(
        onTap: () {
          scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
        },
        child: Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
            color: Colors.blue,
            shape: BoxShape.circle,
          ),
          child: const Icon(Icons.arrow_upward, color: Colors.white),
        ),
      ),
      appBar: AppBar(
        title: const Text(AppStrings.watchList),
        automaticallyImplyLeading: false,
      ),
      body: loading
          ? _loader()
          : watchListMovies.isEmpty
              ? _emptyView()
              : ListView.separated(
                  controller: scrollController,
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                  itemBuilder: (context, index) => FavoriteMovieItem(
                      movie: watchListMovies[index], inWatchList: true),
                  separatorBuilder: (context, index) => const Divider(),
                  itemCount: watchListMovies.length,
                ),
    );
  }

  Widget _loader() {
    return const Center(
      child: SpinKitThreeBounce(
        color: Colors.blue,
        size: 20,
      ),
    );
  }

  Widget _emptyView() {
    return const Center(
      child: Text(AppStrings.noWatchlistMovies,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
    );
  }
}
