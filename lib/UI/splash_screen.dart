import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tmdb_task/bloc/auth/bloc.dart';
import 'package:tmdb_task/main_router.gr.dart';
import 'package:tmdb_task/resources/strings.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final AuthBloc _authBloc = GetIt.instance<AuthBloc>();
  late StreamSubscription _authSub;

  @override
  void initState() {
    _authSub = _authBloc.authStateSubject.listen((state) {
      if (state is CachedSessionDataIs) {
        if (state.accountID == null || state.sessionID == null) {
          AutoRouter.of(context)
              .pushAndPopUntil(const LoginRoute(), predicate: (r) => false);
          return;
        }
        AutoRouter.of(context)
              .pushAndPopUntil(const HomeRoute(), predicate: (r) => false);
      }
    });
    Future.delayed(const Duration(milliseconds: 2000), () {
    _authBloc.dispatch(CachedSessionDataRequested());
    });
    super.initState();
  }

  @override
  void dispose() {
    _authSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black,
        child: const Center(
          child: Text(
            AppStrings.appTitle,
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
