// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:tmdb_task/UI/now_playing_movies_screen.dart';
import 'package:tmdb_task/UI/watch_list_movies_screen.dart';
import 'package:tmdb_task/resources/strings.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int? _current;
  Widget? _body;

  Map<int, Widget> screenMap = {
    0: const NowPlayingMoviesScreen(),
    1: const WatchListMoviesScreen(),
  };

  @override
  void initState() {
    setState(() {
      _current = 0;
      _body = screenMap[_current];
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottomNavBar(),
      body: _body,
    );
  }

  BottomNavigationBar _bottomNavBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.white,
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.grey,
      currentIndex: _current ?? 0,
      onTap: (int selected) {
        _setScreen(selected);
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home,color: _current == 0 ? Colors.blue : Colors.grey,),
          title: Text(AppStrings.nowPlaying,style:TextStyle(color: _current == 0 ? Colors.blue : Colors.grey,)),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite,color: _current == 1 ? Colors.blue : Colors.grey,),
          title: Text(AppStrings.watchList,style:TextStyle(color: _current == 1 ? Colors.blue : Colors.grey,)),
        ),
      ],
    );
  }

  _setScreen(int i) {
    setState(() {
      _current = i;
      _body = screenMap[i];
    });
  }
}
