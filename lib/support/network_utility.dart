import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class NetworkProvider {
  Future<dynamic> get(String link, {Map<String, dynamic>? queryParams}) async {
    try {
      Map<String, dynamic> params = {
        "api_key": "31521ab741626851b73c684539c33b5a"
      };
      if (queryParams != null) {
        params.addAll(queryParams);
      }
      var response = await Dio().get(link, queryParameters: params);
      return response.data;
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future<dynamic> post(String link, Map<String, dynamic> body,
      {Map<String, dynamic>? queryParams}) async {
    try {
      Map<String, dynamic> params = {
        "api_key": "31521ab741626851b73c684539c33b5a"
      };
      if (queryParams != null) {
        params.addAll(queryParams);
      }
      var response = await Dio().post(
        link,
        data: body,
        queryParameters: params,
      );
      debugPrint(response.toString());
      return response.data;
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
