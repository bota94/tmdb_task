// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i6;
import 'package:flutter/material.dart' as _i7;

import 'models/movie.dart' as _i8;
import 'UI/home_screen.dart' as _i5;
import 'UI/login_screen.dart' as _i2;
import 'UI/movie_details_screen.dart' as _i4;
import 'UI/now_playing_movies_screen.dart' as _i3;
import 'UI/splash_screen.dart' as _i1;

class AppRouter extends _i6.RootStackRouter {
  AppRouter([_i7.GlobalKey<_i7.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.SplashScreen());
    },
    LoginRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.LoginScreen());
    },
    NowPlayingMoviesRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.NowPlayingMoviesScreen());
    },
    MovieDetailsRoute.name: (routeData) {
      final args = routeData.argsAs<MovieDetailsRouteArgs>();
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i4.MovieDetailsScreen(
              key: args.key, movie: args.movie, inWatchList: args.inWatchList));
    },
    HomeRoute.name: (routeData) {
      return _i6.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.HomeScreen());
    }
  };

  @override
  List<_i6.RouteConfig> get routes => [
        _i6.RouteConfig(SplashRoute.name, path: '/'),
        _i6.RouteConfig(LoginRoute.name, path: '/login-screen'),
        _i6.RouteConfig(NowPlayingMoviesRoute.name,
            path: '/now-playing-movies-screen'),
        _i6.RouteConfig(MovieDetailsRoute.name, path: '/movie-details-screen'),
        _i6.RouteConfig(HomeRoute.name, path: '/home-screen')
      ];
}

/// generated route for [_i1.SplashScreen]
class SplashRoute extends _i6.PageRouteInfo<void> {
  const SplashRoute() : super(name, path: '/');

  static const String name = 'SplashRoute';
}

/// generated route for [_i2.LoginScreen]
class LoginRoute extends _i6.PageRouteInfo<void> {
  const LoginRoute() : super(name, path: '/login-screen');

  static const String name = 'LoginRoute';
}

/// generated route for [_i3.NowPlayingMoviesScreen]
class NowPlayingMoviesRoute extends _i6.PageRouteInfo<void> {
  const NowPlayingMoviesRoute()
      : super(name, path: '/now-playing-movies-screen');

  static const String name = 'NowPlayingMoviesRoute';
}

/// generated route for [_i4.MovieDetailsScreen]
class MovieDetailsRoute extends _i6.PageRouteInfo<MovieDetailsRouteArgs> {
  MovieDetailsRoute(
      {_i7.Key? key, required _i8.Movie movie, required bool inWatchList})
      : super(name,
            path: '/movie-details-screen',
            args: MovieDetailsRouteArgs(
                key: key, movie: movie, inWatchList: inWatchList));

  static const String name = 'MovieDetailsRoute';
}

class MovieDetailsRouteArgs {
  const MovieDetailsRouteArgs(
      {this.key, required this.movie, required this.inWatchList});

  final _i7.Key? key;

  final _i8.Movie movie;

  final bool inWatchList;
}

/// generated route for [_i5.HomeScreen]
class HomeRoute extends _i6.PageRouteInfo<void> {
  const HomeRoute() : super(name, path: '/home-screen');

  static const String name = 'HomeRoute';
}
